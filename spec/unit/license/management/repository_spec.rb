# frozen_string_literal: true

RSpec.describe License::Management::Repository do
  describe "#item_for" do
    let(:spdx_licenses) { JSON.parse(IO.read('spdx-licenses.json'))['licenses'] }

    context "when mapping a license that refers to opensource.org" do
      it 'parses the SPDX id from the url' do
        spdx_licenses.each do |license|
          spdx_id = license['licenseId']
          url = "https://opensource.org/licenses/#{spdx_id}"
          license = LicenseFinder::License.new(short_name: url, matcher: LicenseFinder::License::NoneMatcher.new, url: url)
          expect(subject.item_for(license)['id']).to eql(spdx_id)
        end
      end

      it 'recognizes `http://www.opensource.org/licenses/mit-license.php`' do
        url = 'http://www.opensource.org/licenses/mit-license.php'
        license = LicenseFinder::License.new(short_name: url, matcher: LicenseFinder::License::NoneMatcher.new, url: url)

        expect(subject.item_for(license)['id']).to eql('MIT')
      end
    end

    context "when mapping a license that refers to nuget.org" do
      it 'parses the SPDX id from the url' do
        spdx_licenses.each do |license|
          spdx_id = license['licenseId']
          url = "https://licenses.nuget.org/#{spdx_id}"
          license = LicenseFinder::License.new(short_name: url, matcher: LicenseFinder::License::NoneMatcher.new, url: url)
          expect(subject.item_for(license)['id']).to eql(spdx_id)
        end
      end
    end

    [
      ['Apache License v2.0', 'Apache-2.0'],
      ['COMMON DEVELOPMENT AND DISTRIBUTION LICENSE (CDDL) Version 1.0', 'CDDL-1.0'],
      ['Common Development and Distribution License 1.1', 'CDDL-1.1'],
      ['CDDL 1.1', 'CDDL-1.1'],
      ['Apache Software License - Version 2.0', 'Apache-2.0'],
      ['ASF 2.0', 'Apache-2.0'],
      ['Eclipse Public License - v 1.0', 'EPL-1.0'],
      ['Eclipse Public License 1.0',  'EPL-1.0'],
      ['Eclipse Public License v1.0', 'EPL-1.0'],
      ['GNU General Public License v2.0 only, with Classpath exception', 'GPL-2.0-only'],
      ['GNU General Public License, version 2', 'GPL-2.0-only'],
      ['GNU Lesser General Public License v2.1 or later', 'LGPL-2.1-or-later'],
      ['GNU Library General Public License v2.1 or later', 'LGPL-2.1+'],
      ['GPL-2.0', 'GPL-2.0-only'],
      ['LGPL-2.1', 'LGPL-2.1'],
      ['GPL2 w/ CPE', 'GPL-2.0-only'],
      ['BSD 3-Clause License', 'BSD-3-Clause'],
      ['The BSD 3-Clause License', 'BSD-3-Clause']
    ].each do |short_name, spdx_id|
      context "when mapping a `#{short_name}` license" do
        let(:license) { LicenseFinder::License.new(short_name: short_name, matcher: LicenseFinder::License::NoneMatcher.new, url: nil) }
        let(:dependency) { double(name: 'x', summary: '', description: '', homepage: '', licenses: [license]) }

        it { expect(subject.item_for(license)['id']).to eql(spdx_id) }
      end
    end

    context 'with BSD license' do
      subject(:item_license) { described_class.new.item_for(license) }

      let(:license) do
        LicenseFinder::License.new(
          short_name: 'BSD',
          other_names: ['BSD4', 'bsd-old', '4-clause BSD', 'BSD-4-Clause', 'BSD 4-Clause', 'BSD License'],
          url: 'http://en.wikipedia.org/wiki/BSD_licenses#4-clause_license_.28original_.22BSD_License.22.29'
        )
      end

      it 'returns general url' do
        expect(item_license['url']).to eq('http://en.wikipedia.org/wiki/BSD_licenses')
      end
    end
  end
end
