# frozen_string_literal: true

require 'spec_helper'

RSpec.describe LicenseFinder::Bundler do
  let(:package_manager) { described_class.new(options) }
  let(:options) { { ignored_groups: [], project_path: project.project_path } }
  let(:project) { ProjectHelper.new }

  before do
    project.mount(dir: project_fixture)
  end

  after do
    project.cleanup
  end

  describe "#current_packages" do
    subject do
      project.chdir do
        package_manager.prepare
        package_manager.current_packages
      end
    end

    context "when scanning a v2.1 bundler project" do
      let(:project_fixture) { fixture_file('ruby/bundler-v2.1') }

      specify { expect(subject.map(&:name)).to match_array(%w[bundler net-hippie]) }
    end

    context "when scanning a v1.17 bundler project" do
      let(:project_fixture) { fixture_file('ruby/bundler-v1.17') }

      specify { expect(subject.map(&:name).sort).to match_array(%w[activemodel activesupport builder bundler concurrent-ruby i18n mini_portile2 minitest net-hippie nokogiri saml-kit thread_safe tilt tzinfo xml-kit xmldsig zeitwerk]) }
    end

    context "when scanning a project with a .ruby-version:2.4.9" do
      let(:project_fixture) { fixture_file('ruby/bundler-ruby-2.4.9-no-lockfile') }

      specify { expect(subject.map(&:name)).to include("saml-kit") }
    end

    context "when scanning a project with a Gemfile that specifies 2.4.9" do
      let(:project_fixture) { fixture_file('ruby/bundler/ruby-2.4.9') }

      specify { expect(subject.map(&:name)).to include("saml-kit") }
    end
  end

  describe "#ruby_version" do
    subject { package_manager.send(:ruby_version) }

    context "when the version of ruby is specified in the Gemfile" do
      let(:project_fixture) { fixture_file('ruby/bundler/ruby-2.4.9') }

      specify { expect(subject).to eql('2.4.9') }
    end

    context "when the version of ruby is specified in a .ruby-version file" do
      let(:project_fixture) { fixture_file('ruby/bundler-ruby-2.4.9-no-lockfile') }

      specify { expect(subject).to eql('2.4.9') }
    end

    context "when the version of ruby is specified in a .tool-versions file" do
      let(:project_fixture) { fixture_file('ruby/bundler/ruby-2.6.0-tool-versions') }

      specify { expect(subject).to eql('2.6.0') }
    end

    context "when a ruby is not specified it uses the default version" do
      let(:project_fixture) { fixture_file('ruby/bundler-v2.1') }

      specify { expect(subject).to eql('2.6.6') }
    end
  end
end
