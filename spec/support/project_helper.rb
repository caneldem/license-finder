# frozen_string_literal: true

class ProjectHelper
  attr_reader :project_path

  def initialize(project_path = License::Management.root.join('tmp').join(SecureRandom.uuid))
    FileUtils.mkdir_p(project_path)
    @project_path = Pathname(project_path)
  end

  def add_file(name, content = nil)
    full_path = project_path.join(name)
    FileUtils.mkdir_p(full_path.dirname)
    IO.write(full_path, block_given? ? yield : content)
  end

  def mount(dir:)
    FileUtils.cp_r("#{dir}/.", project_path)
  end

  def chdir
    Dir.chdir project_path do
      yield
    end
  end

  def clone(repo, branch: 'master')
    if branch.match?(/\b[0-9a-f]{5,40}\b/)
      execute({}, 'git', 'clone', '--quiet', repo, project_path.to_s)
      chdir do
        execute({}, 'git', 'checkout', branch)
      end
    else
      execute({}, 'git', 'clone', '--quiet', '--depth=1', '--single-branch', '--branch', branch, repo, project_path.to_s)
    end
  end

  def scan(env: {})
    chdir do
      return {} unless execute({ 'CI_PROJECT_DIR' => project_path.to_s }.merge(env), "#{License::Management.root.join('run.sh')} analyze .")

      report_path = project_path.join('gl-license-scanning-report.json')
      return {} unless report_path.exist?

      Report.new(report_path.read)
    end
  end

  def execute(env = {}, *args)
    Bundler.with_unbundled_env do
      system(env, *args, exception: true)
    end
  end

  def cleanup
    FileUtils.rm_rf(project_path) if project_path.exist?
  end
end
