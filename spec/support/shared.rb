# frozen_string_literal: true

RSpec.shared_examples "each report version" do |language, package_manager, branch = 'master'|
  ['1.0', '1.1', '2.0', '2.1'].each do |version|
    context "when generating a `#{version}` report for #{package_manager}" do
      subject { runner.scan(env: { 'LM_REPORT_VERSION' => version }) }

      let(:url) { "https://gitlab.com/gitlab-org/security-products/tests/#{language}-#{package_manager}.git" }
      let(:expected_content) { JSON.parse(fixture_file_content("expected/#{language}/#{package_manager}/v#{version}.json")) }

      before do
        runner.clone(url, branch: branch)
      end

      it 'matches the expected report' do
        expect(JSON.pretty_generate(subject.to_h)).to eq(JSON.pretty_generate(expected_content))
        expect(subject).to match_schema(version: version)
      end
    end
  end
end
