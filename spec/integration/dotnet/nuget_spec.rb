# frozen_string_literal: true

require 'spec_helper'

RSpec.describe "nuget" do
  subject { runner.scan(env: env) }

  let(:env) { {} }

  before do
    system("rm -fr /opt/asdf/installs/mono")
    system("rm -fr /opt/gitlab/.nuget/packages")
  end

  include_examples "each report version", "csharp", "nuget-dotnetcore"

  context "when a project has a nuget packages.config file in the root" do
    let(:packages_config) do
      <<-XML
<?xml version="1.0" encoding="utf-8"?>
<packages>
  <package id="Microsoft.CodeDom.Providers.DotNetCompilerPlatform" version="1.0.0" targetFramework="net46" />
  <package id="Microsoft.Net.Compilers" version="1.0.0" targetFramework="net46" developmentDependency="true" />
  <package id="Microsoft.Web.Infrastructure" version="1.0.0.0" targetFramework="net46" />
  <package id="Microsoft.Web.Xdt" version="2.1.1" targetFramework="net46" />
  <package id="Newtonsoft.Json" version="8.0.3" allowedVersions="[8,10)" targetFramework="net46" />
  <package id="NuGet.Core" version="2.11.1" targetFramework="net46" />
  <package id="NuGet.Server" version="2.11.2" targetFramework="net46" />
  <package id="RouteMagic" version="1.3" targetFramework="net46" />
  <package id="WebActivatorEx" version="2.1.0" targetFramework="net46" />
  <package id="jive" version="0.1.0" />
</packages>
      XML
    end

    before do
      runner.add_file('packages.config', packages_config)
    end

    it 'produces a valid report' do
      expect(subject).to match_schema
      expect(subject[:licenses].count).not_to be_zero
      expect(subject.dependency_names).to match_array([
        "Microsoft.CodeDom.Providers.DotNetCompilerPlatform",
        "Microsoft.Net.Compilers",
        "Microsoft.Web.Infrastructure",
        "Microsoft.Web.Xdt",
        "Newtonsoft.Json",
        "NuGet.Core",
        "NuGet.Server",
        "RouteMagic",
        "WebActivatorEx",
        "jive"
      ])
    end

    specify { expect { subject }.to perform_under(60).sec.warmup(0).times }
  end

  context "when a project has multiple nuget packages.config in different sub directories" do
    let(:console_packages) do
      <<-XML
<?xml version="1.0" encoding="utf-8"?>
<packages>
  <package id="jive" version="0.1.0" />
</packages>
      XML
    end

    let(:winforms_packages) do
      <<-XML
<?xml version="1.0" encoding="utf-8"?>
<packages>
  <package id="MvcMailer" version="4.5.0" />
</packages>
      XML
    end

    it 'produces a report including dependencies from each sub directory' do
      runner.add_file('console/packages.config', console_packages)
      runner.add_file('winforms/packages.config', winforms_packages)
      report = runner.scan(env: { 'LICENSE_FINDER_CLI_OPTS' => '--recursive' })

      expect(report).to match_schema
      expect(report.licenses_for('jive')).to match_array(['MIT'])
      expect(report.licenses_for('MvcMailer')).to match_array(['MIT'])
    end
  end

  context "when a project has a dependency that has indirect dependencies" do
    before do
      runner.mount(dir: fixture_file('dotnet/nuget-transient-dependencies'))
    end

    specify 'produces a report that includes the dependencies of each dependency' do
      expect(subject).to match_schema
      expect(subject.dependency_names).to include('Iesi.Collections')
      expect(subject.dependency_names).to include('Remotion.Linq')
      expect(subject.dependency_names).to include('Remotion.Linq.EagerFetching')
      expect(subject.dependency_names).to include("Antlr3.Runtime")
      expect(subject.dependency_names).to include("NHibernate")

      expect(subject.licenses_for('Antlr3.Runtime')).to match_array(["https://raw.githubusercontent.com/antlr/antlrcs/master/license.txt"])
      expect(subject.licenses_for('Iesi.Collections')).to match_array(['unknown'])
      expect(subject.licenses_for('NHibernate')).to match_array(["https://raw.githubusercontent.com/nhibernate/nhibernate-core/master/license.txt"])
      expect(subject.licenses_for('Remotion.Linq')).to match_array(['Apache-2.0'])
      expect(subject.licenses_for('Remotion.Linq.EagerFetching')).to match_array(['LGPL-2.1'])
    end
  end

  context "when scanning a VB.NET project" do
    before do
      runner.mount(dir: fixture_file('dotnet/nuget-vbproj'))
    end

    specify do
      expect(subject).to match_schema
      expect(subject.dependency_names).to match_array(["jive"])
      expect(subject.licenses_for('jive')).to match_array('MIT')
    end
  end

  context "when scanning a F# project" do
    before do
      runner.mount(dir: fixture_file('dotnet/nuget-fsproj'))
    end

    specify do
      expect(subject).to match_schema
      expect(subject.dependency_names).to match_array(["FSharp.Core", "jive"])
      expect(subject.licenses_for('jive')).to match_array('MIT')
    end
  end

  context "when scanning a solution file with multiple projects" do
    before do
      runner.mount(dir: fixture_file('dotnet/nuget-sln'))
    end

    specify do
      expect(subject).to match_schema
      expect(subject.dependency_names).to match_array([
        "FSharp.Core",
        "Microsoft.CSharp",
        "Microsoft.CodeCoverage",
        "Microsoft.NET.Test.Sdk",
        "Microsoft.NETCore.Platforms",
        "Microsoft.NETCore.Platforms",
        "Microsoft.NETCore.Targets",
        "Microsoft.TestPlatform.ObjectModel",
        "Microsoft.TestPlatform.TestHost",
        "NETStandard.Library",
        "Newtonsoft.Json",
        "NuGet.Frameworks",
        "System.Collections",
        "System.Diagnostics.Debug",
        "System.Diagnostics.Tools",
        "System.Dynamic.Runtime",
        "System.Globalization",
        "System.IO",
        "System.IO.FileSystem",
        "System.IO.FileSystem.Primitives",
        "System.Linq",
        "System.Linq.Expressions",
        "System.ObjectModel",
        "System.Reflection",
        "System.Reflection.Emit",
        "System.Reflection.Emit.ILGeneration",
        "System.Reflection.Emit.Lightweight",
        "System.Reflection.Extensions",
        "System.Reflection.Primitives",
        "System.Reflection.TypeExtensions",
        "System.Resources.ResourceManager",
        "System.Runtime",
        "System.Runtime.Extensions",
        "System.Runtime.Handles",
        "System.Runtime.InteropServices",
        "System.Runtime.Serialization.Primitives",
        "System.Text.Encoding",
        "System.Text.Encoding.Extensions",
        "System.Text.RegularExpressions",
        "System.Threading",
        "System.Threading.Tasks",
        "System.Threading.Tasks.Extensions",
        "System.Xml.ReaderWriter",
        "System.Xml.XDocument",
        "coverlet.collector",
        "xunit",
        "xunit.abstractions",
        "xunit.analyzers",
        "xunit.assert",
        "xunit.core",
        "xunit.extensibility.core",
        "xunit.extensibility.execution",
        "xunit.runner.visualstudio"
      ])

      expect(subject.licenses_for('NuGet.Frameworks')).to match_array('Apache-2.0')
      expect(subject.licenses_for('FSharp.Core')).to match_array('MIT')
    end
  end

  context "when fetching dependencies from a custom registry" do
    before do
      runner.mount(dir: fixture_file('dotnet/nuget-custom-tls'))
    end

    context "when the CA certificate is provided" do
      let(:env) { { 'ADDITIONAL_CA_CERT_BUNDLE' => x509_certificate.read } }

      specify do
        expect(subject).to match_schema
        expect(subject.dependency_names).to match_array(["jive"])
        expect(subject.licenses_for('jive')).to match_array('MIT')
      end
    end

    context "when the CA certificate is NOT provided" do
      let(:env) { {} }

      specify { expect(subject).to match_schema }
    end
  end
end
