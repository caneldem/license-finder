# frozen_string_literal: true

require 'spec_helper'

RSpec.describe "cargo" do
  subject { runner.scan(env: env) }

  let(:env) { {} }

  before do
    install_dir = '/opt/asdf/installs/rust'
    system("rm -r #{install_dir}") if Dir.exist?(install_dir)
  end

  include_examples "each report version", "rust", "cargo"

  context "when scanning a cargo project" do
    before do
      runner.mount(dir: fixture_file('rust/cargo/hello_world'))
    end

    specify { expect(subject).to match_schema }

    specify do
      expect(subject.dependency_names).to match_array(%w[libc time])
    end

    specify { expect(subject.licenses_for('libc')).to match_array(['MIT', 'Apache-2.0']) }
    specify { expect(subject.licenses_for('time')).to match_array(['Apache-2.0', 'MIT']) }
  end

  context "when fetching dependencies from a custom registry" do
    before do
      runner.mount(dir: fixture_file('rust/cargo/custom-tls'))
    end

    context "when the CA certificate is provided" do
      let(:env) { { 'ADDITIONAL_CA_CERT_BUNDLE' => x509_certificate.read } }

      specify do
        expect(subject).to match_schema
        expect(subject.dependency_names).to match_array(['libc'])
        expect(subject.licenses_for('libc')).to match_array(['MIT', 'Apache-2.0'])
      end
    end

    context "when the CA certificate is NOT provided" do
      let(:env) { {} }

      specify { expect(subject).to match_schema }
    end
  end

  context "when scanning a project with dev dependencies" do
    before do
      runner.mount(dir: fixture_file('rust/cargo/dev-dependencies'))
    end

    it 'excludes them from the report' do
      expect(subject).to match_schema
      expect(subject.dependency_names).to include('diesel')
      expect(subject.dependency_names).not_to include('pretty_assertions')
    end
  end

  context "when scanning https://gitlab.com/inko-lang/inko.git" do
    let(:env) { { 'LICENSE_FINDER_CLI_OPTS' => '--aggregate-paths=vm compiler' } }

    before do
      runner.clone('https://gitlab.com/inko-lang/inko.git')
    end

    specify { expect(subject).to match_schema }
    specify { expect { subject }.to perform_under(60).sec.warmup(0).times }
  end
end
