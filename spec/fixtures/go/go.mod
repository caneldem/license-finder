module example

go 1.12

require (
	github.com/dimfeld/httptreemux/v5 v5.0.2
	github.com/go-logfmt/logfmt v0.5.0
	github.com/google/uuid v1.1.1
	github.com/stretchr/testify v1.4.0
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
)
