# frozen_string_literal: true

require 'json'
require 'logger'
require 'pathname'
require 'spandx'
require 'yaml'

require 'license_finder'
require 'license/management/loggable'
require 'license/management/verifiable'
require 'license/management/nuspec'
require 'license/management/repository'
require 'license/management/report'
require 'license/management/shell'
require 'license/management/tool_box'
require 'license/management/version'

require 'license/finder/ext'

module License
  module Management
    def self.root
      Pathname.new(File.dirname(__FILE__)).join('../..')
    end

    def self.logger
      @logger ||= Logger.new(STDOUT, level: ENV.fetch('LOG_LEVEL', Logger::WARN)).tap do |x|
        x.formatter = proc do |_severity, _datetime, _progname, message|
          prefix = message.to_s.start_with?("\n") ? "" : "[v#{VERSION}] "
          "#{prefix}#{message}\n"
        end
      end
    end

    def self.shell
      @shell ||= Shell.new
    end
  end
end
