# frozen_string_literal: true

module LicenseFinder
  class Conan
    def possible_package_paths
      [project_path.join('conanfile.txt')]
    end

    def prepare
      within_project_path do
        tool_box.install(tool: :python)
        shell.execute([:conan, :install, '--build=missing', '.'], env: default_env)
        shell.execute([:conan, :inspect, '.'], env: default_env)
      end
    end

    def current_packages
      stdout, _stderr, status = within_project_path do
        shell.execute([:conan, :info, '-j', '/dev/stdout', '.'], env: default_env)
      end
      return [] unless status.success?

      parse(stdout.lines[0]).map { |dependency| map_from(dependency) }.compact
    end

    private

    def extract_name_version_from(name)
      name.split('@', 2).first.split('/', 2)
    end

    def map_from(dependency)
      name, version = extract_name_version_from(dependency['reference'])
      return if name == 'conanfile.txt'

      Dependency.new('Conan', name, version, spec_licenses: licenses_for(dependency), detection_path: detected_package_path)
    end

    def licenses_for(dependency)
      dependency['license']
    end

    def parse(line)
      JSON.parse(line)
    end

    def default_env
      @default_env ||= {
        'CONAN_CACERT_PATH' => ENV.fetch('CONAN_CACERT_PATH', '/etc/ssl/certs/ca-certificates.crt'),
        'CONAN_LOGGING_LEVEL' => ENV['LOG_LEVEL'],
        'CONAN_LOGIN_USERNAME' => ENV.fetch('CONAN_LOGIN_USERNAME', 'ci_user'),
        'CONAN_LOG_RUN_TO_OUTPUT' => '1',
        'CONAN_NON_INTERACTIVE' => '1',
        'CONAN_PASSWORD' => ENV.fetch('CONAN_PASSWORD', ENV['CI_JOB_TOKEN']),
        'CONAN_PRINT_RUN_COMMANDS' => '1',
        'CONAN_REQUEST_TIMEOUT' => '5',
        'CONAN_RETRY' => '1',
        'CONAN_RETRY_WAIT' => jitter,
        'CONAN_USER_HOME' => Dir.pwd
      }
    end

    def jitter
      rand(5).to_s
    end
  end
end
