# frozen_string_literal: true

module License
  module Management
    VERSION = '3.28.0'
  end
end
